
# Ejecución del Código

## Descripción del Código

Este código en Python utiliza la biblioteca Scapy para capturar y analizar tramas Ethernet, con especial énfasis en el manejo de etiquetas MPLS (Multiprotocol Label Switching). El programa define funciones para procesar tramas Ethernet y etiquetas MPLS, y luego captura paquetes de una interfaz de red especificada.


## Windows

1. **Instalar Python:**
   - Descarga e instala Python desde [python.org](https://www.python.org/downloads/).

2. **Abrir una Terminal (cmd):**
   - Abre una terminal de Windows.

3. **Crear un Entorno Virtual:**
   ```bash
   python -m venv venv
   ```

4. **Activar el Entorno Virtual:**
   ```bash
   .\venv\Scripts\activate
   ```

5. **Instalar Dependencias:**
   ```bash
   pip install scapy
   ```

6. **Ejecutar el Código:**
   ```bash
   python Punto_1.py
   ```

7. **Desactivar el Entorno Virtual:**
   ```bash
   deactivate
   ```

## Linux

1. **Instalar Python:**
   - Asegúrate de tener Python instalado. Si no lo tienes, puedes instalarlo con el administrador de paquetes de tu distribución.

2. **Abrir una Terminal:**
   - Abre una terminal.

3. **Crear un Entorno Virtual:**
   ```bash
   python3 -m venv venv
   ```

4. **Activar el Entorno Virtual:**
   ```bash
   source venv/bin/activate
   ```

5. **Instalar Dependencias:**
   ```bash
   pip install scapy
   ```

6. **Ejecutar el Código:**
   ```bash
   python Punto_1.py
   ```

7. **Desactivar el Entorno Virtual:**
   ```bash
   deactivate
   ```

## Configuración de la Interfaz de Red
El parámetro iface en la función sniff() especifica la interfaz de red desde la cual se capturarán los paquetes. Asegúrate de cambiar el valor de iface a la interfaz de red de tu equipo.

Ejemplo: iface="enp0s3"

## Ejemplo del Resultado
Al ejecutar el código, deberías ver la información de las tramas Ethernet capturadas, incluyendo detalles sobre las etiquetas MPLS si están presentes.

```
INICIO
Ethernet Frame:
Source MAC: 00:11:22:33:44:55
Destination MAC: 66:77:88:99:AA:BB
Ethernet Type: 0x8847
MPLS Label Stack:
MPLS Label: 100
Exp: 0
TTL: 32
Next Layer Type: IP
FINALIZADO
```