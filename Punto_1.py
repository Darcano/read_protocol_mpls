from scapy.all import *

def process_ethernet_frame(frame):
    print("Ethernet Frame:")
    print(f"Source MAC: {frame.src}")
    print(f"Destination MAC: {frame.dst}")

    if hasattr(frame, 'type'):
        print(f"Ethernet Type: {hex(frame.type)}")
        # Proceso MPLS si esta presente
        if frame.type == 0x8847:  # MPLS
            process_mpls(frame.payload)
        else:
            print("No MPLS")
    else:
        print("No Ethernet Type")

def process_mpls(packet):
    print("MPLS Label Stack:")
    while MPLS in packet:
        mpls_label = packet[MPLS]
        print(f"MPLS Label: {mpls_label.label}")
        print(f"Exp: {mpls_label.exp}")
        print(f"TTL: {mpls_label.ttl}")
        packet = mpls_label.payload

    # Procesar la carga útil después de las etiquetas MPLS
    print(f"Next Layer Type: {type(packet).__name__}")

# Captura de paquetes
def capture_packets():
    sniff(iface="enx207bd2528bc4", prn=lambda x: process_ethernet_frame(x), store=0)

# Llamamos a la función para capturar paquetes
print("INICIO")
capture_packets()
print("FINALIZADO")
